package com.lansingbs.samitivejhome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SamitivejHomeWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamitivejHomeWebApplication.class, args);
	}
}
