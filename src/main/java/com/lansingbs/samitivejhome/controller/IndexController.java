package com.lansingbs.samitivejhome.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/test")
public class IndexController {

    @GetMapping(value = "")
    public ModelAndView test() {
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

}
